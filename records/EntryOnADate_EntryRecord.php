<?php
namespace Craft;

class EntryOnADate_EntryRecord extends BaseRecord
{
  public function getTableName()
  {
    return 'entryonadate_entries';
  }

  protected function defineAttributes()
  {
    return array(
      'date'    => AttributeType::DateTime,
      'entryId' => AttributeType::Number,
      'userId'  => AttributeType::Number,
      'count'   => AttributeType::Number,
      'sum'     => AttributeType::String
    );
  }
}
