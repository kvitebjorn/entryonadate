<?php
namespace Craft;

class EntryOnADate_EntriesController extends BaseController {
  protected $allowAnonymous = true;
  public function actionSaveEntry() {
    $this->requirePostRequest();
    if (isset($_POST['eoad_count']) && isset($_POST['eoad_sum'])) {
      $this->returnJson(craft()->entryOnADate_entries->saveEntry($_POST['eoad_entry_id'], $_POST['eoad_date'], $_POST['eoad_count'], $_POST['eoad_sum']));
    } else {
      $this->returnJson(craft()->entryOnADate_entries->saveEntry($_POST['eoad_entry_id'], $_POST['eoad_date']));
    }
  }

  public function actionDeleteEntry() {
    $this->requirePostRequest();
    $this->returnJson(craft()->entryOnADate_entries->deleteEntry($_POST['eoad_date']));
  }
}
