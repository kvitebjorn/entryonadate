<?php
namespace Craft;

class EntryOnADateVariable {
  public function entries($startDate = null, $endDate = null) {
    return craft()->entryOnADate_entries->getEntries($startDate, $endDate);
  }
}
