<?php
namespace Craft;

class EntryOnADate_EntriesService extends BaseApplicationComponent {

  public function saveEntry($entryId, $date, $count = 1, $sum = 0) {


    $criteria = craft()->elements->getCriteria(ElementType::Entry);
    $criteria->id = $entryId;

    if (count($criteria) == 0) {
      return array(
        'success' => false,
        'reason' => 'entry_does_not_exist'
      );
    }

    if (!is_numeric($count)) {
      return array(
        'success' => false,
        'reason' => 'count_must_be_numeric'
      );
    }

    if (!is_numeric($sum)) {
      return array(
        'success' => false,
        'reason' => 'sum_must_be_numeric'
      );
    }

    try {
      $date = new DateTime($date);
      $date->setTime(5, 5, 5);
      $startDate = new DateTime($date);
      $startDate->setTime(0, 0, 0);
      $endDate = new DateTime($date);
      $endDate->setTime(23, 59, 59);
    } catch (Exception $e) {
      return array(
        'success' => false,
        'reason' => 'invalid_date'
      );
    }

    $existing = EntryOnADate_EntryRecord::model()->findAll(
      'userId = ? and date >= ? and date <= ? order by date',
      array(
        craft()->userSession->getUser()->id,
        $startDate,
        $endDate
      )
    );

    foreach ($existing as $e) {
      EntryOnADate_EntryRecord::model()->deleteByPk($e->id);
    }

    $record = new EntryOnADate_EntryRecord();
    $record->setAttributes(array(
      'userId' => craft()->userSession->getUser()->id,
      'entryId' => $entryId,
      'date' => $date,
      'count' => $count,
      'sum' => $sum
    ));

    if ($record->save()) {
      return array('success' => true);
    } else {
      return array(
        'success' => false,
        'reason' => 'could_not_save_entry'
      );
    }
  }

  public function deleteEntry($date) {
    try {
      $date = new DateTime($date);
      $date->setTime(0, 0, 0);
      $endDate = new DateTime($date);
      $endDate->setTime(23, 59, 59);
    } catch (Exception $e) {
      return array(
        'success' => false,
        'reason' => 'invalid_date'
      );
    }

    $existing = EntryOnADate_EntryRecord::model()->findAll(
      'userId = ? and date >= ? and date <= ? order by date',
      array(
        craft()->userSession->getUser()->id,
        $date,
        $endDate
      )
    );

    foreach ($existing as $e) {
      EntryOnADate_EntryRecord::model()->deleteByPk($e->id);
    }

    return array(
      'success' => true
    );
  }

  public function getEntries($startDate = null, $endDate = null) {
    if (craft()->userSession->isGuest()) {
      return array();
    }

    if ($startDate) {
      if (!$endDate) {
        $endDate = $startDate;
      }
      try {
        $sd = new DateTime($startDate);
        $sd->setTime(0, 0, 0);
        $ed = new DateTime($endDate);
        $ed->setTime(23, 59, 59);

        $records = EntryOnADate_EntryRecord::model()->findAll(
          'userId = ? and date >= ? and date <= ? order by date',
          array(
            craft()->userSession->getUser()->id,
            $sd,
            $ed
          )
        );

        $result = array();

        foreach ($records as $record) {
          $result[$record->date->format('Y-m-d')] = $record;
        }
        return $result;
      } catch (Exception $e) {
        return array();
      }
    } else {
      return EntryOnADate_EntryRecord::model()->findAllByAttributes(
        array('userId' => craft()->userSession->getUser()->id),
        array('order' => 'date')
      );
    }
  }
}
