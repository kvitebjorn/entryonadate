# Entry on a date

For å bruke i en mal:

```
{% set things = craft.entryonadate.entries('2017-07-03', '2017-07-09') %}

{% if things['2017-07-05'] is defined %}
  {% set entry = craft.entries.id(things['2017-07-05'].entryId).first %}
  {{ entry.title }}
{% endif %}
```


Lage nye entries:

```
$.ajax(
  '/actions/entryOnADate/entries/saveEntry',
  {
    eoad_entry_id: 23,
    eoad_date:     '2017-08-11'
  }
);
```

Slette entries:

```
$.ajax(
  '/actions/entryOnADate/entries/deleteEntry',
  {
    eoad_id: 1
  }
);
```
