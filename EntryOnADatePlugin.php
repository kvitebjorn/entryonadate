<?php
namespace Craft;

class EntryOnADatePlugin extends BasePlugin
{
  function getName()
  {
    return Craft::t('Entry on a Date');
  }

  function getVersion()
  {
    return '1.0';
  }

  function getDeveloper()
  {
    return 'Kvitebjørn Designbyrå AS';
  }

  function getDeveloperUrl()
  {
    return 'https://kvitebjorn.com';
  }
}
